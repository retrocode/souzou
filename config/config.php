<?php
/**
 * SAMPLE CONFIG - Remove .example from the filename to use
 * config_dev.php - Development settings
 *
 * Override settings (inherits from the main config file)
 */
return [
    'db'                 => [
        'host'     => 'localhost',
        'port'     => 3306,
        'username' => 'username',
        'password' => 'password',
        'database' => 'databaseName',
    ],
    'email'              => [
        'template'      => null, // MyEmailTemplate::class
        'smtp_host'     => 'email.host.com',
        'smtp_port'     => 587,
        'smtp_username' => 'email@test.com',
        'smtp_password' => 'pasword',
        'from_address'  => 'noreply@test.com',
        'from_name'     => 'FromName',
        'debug'         => true,
        'smtp_auth'     => true,
        // AutoTLS - true, false or null to unset
        'smtp_auto_tls' => null,
        'is_html'       => true,
    ],
    'app'                => [
        'name'      => 'My App',
        'base_url'  => 'https://my-app.com',
        'media_url' => 'https://my-app.com/media',
        'debug'     => false,
        'logFile'   => '/tmp/app.log',
        'base_path' => __DIR__ . '/../souzou/',
        // TODO: Remove this (remove from templates...)
    ],
    'di_mappings'        => [
        // abstractClassName => concreteClassOrInstanceCreatorCallable
        // LoggerInterface::class => function() { return new FileLogger('/path/to/file.txt'); }
        // LoggerInterface::class => CliLogger::class
        \Retrocode\Souzou\Service\Logger::class                            => \Retrocode\Souzou\Service\Logger\NullLogger::class,
        \Retrocode\Souzou\Service\HtmlTemplateEngine\TemplateEngine::class => \Retrocode\Souzou\Service\HtmlTemplateEngine\BladeOneTemplate::class
    ],
    'middleware'         => [
        // You can override middleware from the middleware file in the dev_ version
        // Alternatively, put this in the middleware.php file
    ],
    'cli_commands'       => [
        'help'        => \Retrocode\Souzou\CliCommand\HelpCommand::class,
        'clear_cache' => \Retrocode\Souzou\CliCommand\ClearTemplateCacheCliCommand::class
    ],
    'routes'             => [
        // You can override routes from the routes file in the dev_ version
        // The constructors will have their contents injected (if they can be instantiated)
        // [ 'GET:/home' => HomeController:class ]
    ],
    'argument_resolvers' => [
        // Create resolvers for DI of scalar values from IResolveValues classes (value getters)
        // and by parsing the argument name, value, type and class
        \Retrocode\Souzou\Service\AppSettings::class => function (ReflectionParameter                       $parameter,
                                                                  string                                    $className,
                                                                  \Retrocode\Souzou\Contract\IResolveValues $appSettings) {
            if ($parameter->getType()->getName() !== 'string') {
                throw new \Retrocode\Souzou\Exception\ValueNotResolvedException('String params are only supported');
            }

            if ($parameter->isDefaultValueAvailable()) {
                // Does the argument look like a config value (i.e. to use with AppSettings)...
                if (str_starts_with($parameter->name, 'config')) {
                    return $appSettings->get($parameter->getDefaultValue(), $parameter->getDefaultValue());
                } else {
                    return $parameter->getDefaultValue();
                }
            }
        }
    ]
];
