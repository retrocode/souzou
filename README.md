# Souzou PHP Framework

Structure of project - note this is only a suggestion, no specific structure is required.

- config
    - config.php
    - config_dev.php  // Used if DEV=true is set in .env file
    - middleware.php
    - routes.php
- src (MyApp)
    - Command
        - MyCommand.php
- index.php
- composer.json

## Installation

Composer is recommended, so setup in the usual way...

```shell
composer init
```

Add the following to the composer.json

```
  "require": {
    "retrocode/souzou": "@dev",
    ...
  },
  "repositories": [
    {
      "type": "vcs",
      "url": "https://gitlab.com/retrocode/souzou.git"
      ...
    }
```

Note: if you want to develop the framework, you can use composer symlinking, but using `composer.dev.json` and setting the repository to `path` and `/path/to/souzou` and install with `composer install --dev`

## Contents of .env
```
DEV=true
```

## Contents of index.php

This is an example for local testing - you're likely to want to turn off displaying errors for any live version
```PHP
<?php

use Dotenv\Dotenv;
use Retrocode\Souzou\Framework;

require 'vendor/autoload.php';

// Setup DotEnv (`composer require vlucas/phpdotenv`)
if (file_exists('.env')) {
    $dotenv = Dotenv::createImmutable('.');
    $dotenv->load();
}

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


$isDevMode = $_ENV[ 'DEV' ] ?? false;
$framework = new Framework(__DIR__ . '/config', $isDevMode);

$framework->run();
```