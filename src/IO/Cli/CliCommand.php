<?php

namespace Retrocode\Souzou\IO\Cli;

abstract class CliCommand
{

    protected CliInput  $input;
    protected CliOutput $output;

    public function withInput(CliInput $cliInput): self
    {
        $this->input = $cliInput;

        return $this;
    }

    public function withOutput(CliOutput $cliOutput): self
    {
        $this->output = $cliOutput;

        return $this;
    }

    abstract public function run(CliRequest $request): int;
}