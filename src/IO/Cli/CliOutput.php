<?php

namespace Retrocode\Souzou\IO\Cli;

class CliOutput
{
    private $colors = [
        'black' => '0;30',
        'dark_gray' => '1;30',
        'red' => '0;31',
        'light_red' => '1;31',
        'green' => '0;32',
        'light_green' => '1;32',
        'brown' => '0;33',
        'yellow' => '1;33',
        'blue' => '0;34',
        'light_blue' => '1;34',
        'purple' => '0;35',
        'light_purple' => '1;35',
        'cyan' => '0;36',
        'light_cyan' => '1;36',
        'light_gray' => '0;37',
        'white' => '1;37',
    ];

    public function info(string $message, ?string $color = null)
    {
        $this->output($message, $color);
    }

    public function error(string $message, string $color = 'red')
    {
        $this->output($message, $color, STDERR);
    }

    private function output(string $message, ?string $color = null, $stream = STDOUT)
    {
        if ($color && isset($this->colors[$color])) {
            $message = sprintf("\033[%sm%s\033[0m", $this->colors[ $color ], $message);
        }

        fwrite($stream, $message . PHP_EOL);
    }
}