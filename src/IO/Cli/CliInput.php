<?php

namespace Retrocode\Souzou\IO\Cli;

class CliInput
{
    public function ask(string $prompt): ?string
    {
        echo $prompt . ': ';
        $handle = fopen('php://stdin', 'r');
        $line   = trim(fgets($handle));
        fclose($handle);

        return $line !== '' ? $line : null;
    }

    public function confirm($message)
    {
        $answer = $this->ask($message . ' (y/n): ');

        return strtolower($answer) === 'y';
    }
}