<?php

namespace Retrocode\Souzou\IO\Cli;

use Retrocode\Souzou\Contract\IResolveValues;

class CliRequest implements IResolveValues
{
    private array $options   = [];
    private array $arguments = [];

    public function __construct()
    {
        $args = $_SERVER[ 'argv' ];
        array_shift($args); // Remove the script name from the arguments

        foreach ($args as $arg) {
            if (str_starts_with($arg, '--')) {
                // Long option format: --option value
                $option                   = substr($arg, 2);
                $value                    = $this->getNextArgument($args);
                $this->options[ $option ] = $value;
            } elseif (str_starts_with($arg, '-')) {
                // Short option format: -o=value
                $option                = substr($arg, 1);
                $parts                 = explode('=', $option, 2);
                $key                   = $parts[ 0 ];
                $value                 = $parts[ 1 ] ?? true;
                $this->options[ $key ] = $value;
            } else {
                // Argument format: argumentA argumentB
                $this->arguments[] = $arg;
            }
        }
    }

    private function getNextArgument(array &$args): mixed
    {
        $value = array_shift($args);
        if ($value === null || strpos($value, '-') === 0) {
            // No value provided or next argument is an option
            return true;
        }

        return $value;
    }

    public function getOption($key, $default = null): mixed
    {
        return $this->options[ $key ] ?? $default;
    }

    public function hasArgument(string $name): bool
    {
        return in_array($name, $this->arguments);
    }

    public function getArgument($index, $default = null): mixed
    {
        return $this->arguments[ $index ] ?? $default;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function getArguments(): array
    {
        return $this->arguments;
    }

    public function get(string $key, mixed $defaultValue): mixed
    {
        return $this->getOption($key, $defaultValue);
    }
}