<?php

namespace Retrocode\Souzou\IO\Http;

/**
 * Represents an HTTP request.
 *
 * This class provides an object-oriented representation of an HTTP request.
 * It encapsulates the request data, including the HTTP method, URI, headers,
 * query parameters, and request body.
 *
 * Important: note that requests are not filtered at this level, so be sure
 * to filter for invalid / rogue data in the implementation of the handler
 */
class HttpRequest implements \JsonSerializable
{
    /** @var string */
    private $method;
    /** @var string */
    private $path;
    /** @var string[] */
    private $queryParams;
    /** @var string[] */
    private $postParams;
    /** @var string[] */
    private $headers;

    public function __construct()
    {
        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $this->queryParams = $_GET;
        // Handle JSON as well as standard posted data
        $this->postParams = empty($_POST) ? json_decode(file_get_contents('php://input'), true) : $_POST;
        $this->headers = $this->getHeaders();
    }

    /**
     * Get the HTTP method of the request.
     *
     * @return string The HTTP method (e.g., GET, POST, PUT, DELETE).
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * Get the URI of the request.
     *
     * @return string The request URI.
     */
    public function getPath(): string
    {
        return $this->path;
    }

    public function getQueryParams(): array
    {
        return $this->queryParams;
    }

    public function getPostParams(): array
    {
        return $this->postParams;
    }

    /**
     * Get all headers from the request.
     *
     * @return array An associative array of header names and their values.
     */
    public function getHeaders(): array
    {
        $headers = [];
        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) === 'HTTP_') {
                $headerName = str_replace('_', '-', substr($name, 5));
                $headers[$headerName] = $value;
            }
        }
        return $headers;
    }

    /**
     * Get a specific header from the request.
     *
     * @param string $name The name of the header.
     * @param string|null $default The default value to return if the header is not found.
     * @return string|null The value of the header, or the default value if not found.
     */
    public function getHeader(string $name, string $default = null): string
    {
        return $this->headers[$name] ?? $default;
    }

    /**
     * Get a query parameter from the request.
     *
     * @param string $name The name of the query parameter.
     * @param string|null $default The default value to return if the parameter is not found.
     * @return string|null The value of the query parameter, or the default value if not found.
     */
    public function getQueryParam(string $name, string $default = null): ?string
    {
        return $this->queryParams[$name] ?? $default;
    }

    /**
     * Get a POST parameter from the request.
     *
     * @param string $name The name of the posted parameter.
     * @param string|null $default The default value to return if not found.
     * @return string|null The value of the query parameter, or the default value if not found.
     */
    public function getPostParam(string $name, string $default = null): ?string
    {
        return $this->postParams[$name] ?? $default;
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}
