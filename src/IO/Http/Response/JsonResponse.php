<?php

namespace Retrocode\Souzou\IO\Http\Response;

use Retrocode\Souzou\IO\Http\HttpResponse;

/**
 * Represents a JSON response.
 *
 * This class provides an object-oriented representation of an HTTP response.
 * It encapsulates the response data, including the HTTP status code, headers,
 * and response body.
 */
class JsonResponse extends HttpResponse
{
    /** @var array */
    private $body;

    public function __construct(array $body, int $statusCode = 200)
    {
        $this->statusCode = $statusCode;
        $this->headers    = [ 'Content-Type'  => 'application/json',
                              'Connection'    => 'keep-alive',
                              'Cache-Control' => 'no-cache',
                              'Date'          => gmdate('D, d M Y H:i:s T'), ];
        $this->body       = $body;
    }

    /**
     * Get the response body.
     *
     * @return string The response body as a string.
     */
    public function getBody(): string
    {
        // TODO: This may not need to be "pretty printed", but easier for viewing while testing
        return json_encode($this->body, JSON_PRETTY_PRINT);
    }

    /**
     * Send the response to the client.
     *
     * This method sends the HTTP headers and outputs the response body.
     *
     * @return void
     */
    public function send(): void
    {
        $this->sendHeaders();
        $this->sendBody();
    }

    protected function sendHeaders(): void
    {
        http_response_code($this->statusCode);
        foreach ($this->headers as $name => $value) {
            header("$name: $value");
        }
    }

    protected function sendBody(): void
    {
        echo $this->getBody();
    }
}