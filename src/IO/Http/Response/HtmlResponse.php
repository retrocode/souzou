<?php

namespace Retrocode\Souzou\IO\Http\Response;

use Retrocode\Souzou\IO\Http\HttpResponse;

/**
 * Represents an HTTP response.
 *
 * This class provides an object-oriented representation of an HTTP response.
 * It encapsulates the response data, including the HTTP status code, headers,
 * and response body.
 */
class HtmlResponse extends HttpResponse
{
    /** @var string */
    private $body;

    public function __construct(string $body = '', int $statusCode = 200)
    {
        $this->statusCode = $statusCode;
        $this->headers    = [ 'Content-Type' => 'text/html; charset=UTF-8',
                              'Connection'   => 'keep-alive',
                              'Date'         => gmdate('D, d M Y H:i:s T'), ];
        $this->body       = $body;
    }

    /**
     * Get the response body.
     *
     * @return string The response body as a string.
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * Send the response to the client.
     *
     * This method sends the HTTP headers and outputs the response body.
     *
     * @return void
     */
    public function send(): void
    {
        $this->setHeader('Content-Length', strlen($this->getBody()));
        $this->sendHeaders();
        $this->sendBody();
    }

    protected function sendHeaders(): void
    {
        http_response_code($this->statusCode);
        foreach ($this->headers as $name => $value) {
            header("$name: $value");
        }
    }

    protected function sendBody(): void
    {
        echo $this->body;
    }
}