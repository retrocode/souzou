<?php

namespace Retrocode\Souzou\IO\Http;

abstract class HttpResponse
{
    public const STATUS_OK = 200;

    public const STATUS_BAD_REQUEST = 400;
    public const STATUS_UNAUTHORIZED = 401;
    public const STATUS_PAYMENT_REQUIRED = 402;
    public const STATUS_FORBIDDEN = 403;
    public const STATUS_NOT_FOUND = 404;
    public const STATUS_METHOD_NOT_ALLOWED = 405;
    public const STATUS_NOT_ACCEPTABLE = 406;
    public const STATUS_REQUEST_TIMEOUT = 408;
    public const STATUS_TOO_MANY_REQUESTS = 429;

    public const STATUS_INTERNAL_SERVER_ERROR = 500;

    /** @var int|null */
    protected $statusCode;
    /** @var array */
    protected $headers;

    /**
     * Get the HTTP status code of the response.
     *
     * @return int The HTTP status code.
     */
    final public function getStatusCode(): ?int
    {
        return $this->statusCode;
    }

    /**
     * Set the HTTP status code of the response.
     *
     * @param int $statusCode The HTTP status code to set.
     * @return self The current Response instance for method chaining.
     */
    final public function setStatusCode(int $statusCode): self
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Get all headers from the response.
     *
     * @return array An associative array of header names and their values.
     */
    final public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * Set a header in the response.
     *
     * @param string $name The name of the header.
     * @param string $value The value of the header.
     * @return self The current Response instance for method chaining.
     */
    final public function setHeader(string $name, string $value): self
    {
        $this->headers[$name] = $value;

        return $this;
    }

    final public function removeHeader(string $name): void
    {
        unset($this->headers[$name]);
    }

    abstract public function getBody(): string;

    /**
     * Send the response to the client.
     *
     * This method sends the HTTP headers and outputs the response body.
     *
     * @return void
     */
    public function send(): void
    {
        $this->sendHeaders();
        $this->sendBody();
    }

    /**
     * Dispatch (echo) header responses for the requester (client)
     */
    abstract protected function sendHeaders(): void;

    /**
     * Dispatch (echo) the main body for the requester (client)
     */
    abstract protected function sendBody(): void;


    // Other possible status codes...
    //    public const STATUS_CREATED = 201;
    //    public const STATUS_ACCEPTED = 202;
    //    public const STATUS_NON_AUTHORITATIVE_INFORMATION = 203;
    //    public const STATUS_NO_CONTENT = 204;
    //    public const STATUS_RESET_CONTENT = 205;
    //    public const STATUS_PARTIAL_CONTENT = 206;
    //    public const STATUS_MULTI_STATUS = 207;
    //    public const STATUS_ALREADY_REPORTED = 208;
    //    public const STATUS_IM_USED = 226;
    //    public const STATUS_MULTIPLE_CHOICES = 300;
    //    public const STATUS_MOVED_PERMANENTLY = 301;
    //    public const STATUS_FOUND = 302;
    //    public const STATUS_SEE_OTHER = 303;
    //    public const STATUS_NOT_MODIFIED = 304;
    //    public const STATUS_USE_PROXY = 305;
    //    public const STATUS_TEMPORARY_REDIRECT = 307;
    //    public const STATUS_PERMANENT_REDIRECT = 308;
    //    public const STATUS_PROXY_AUTHENTICATION_REQUIRED = 407;
    //    public const STATUS_CONFLICT = 409;
    //    public const STATUS_GONE = 410;
    //    public const STATUS_LENGTH_REQUIRED = 411;
    //    public const STATUS_PRECONDITION_FAILED = 412;
    //    public const STATUS_PAYLOAD_TOO_LARGE = 413;
    //    public const STATUS_URI_TOO_LONG = 414;
    //    public const STATUS_UNSUPPORTED_MEDIA_TYPE = 415;
    //    public const STATUS_RANGE_NOT_SATISFIABLE = 416;
    //    public const STATUS_EXPECTATION_FAILED = 417;
    //    public const STATUS_IM_A_TEAPOT = 418;
    //    public const STATUS_MISDIRECTED_REQUEST = 421;
    //    public const STATUS_UNPROCESSABLE_ENTITY = 422;
    //    public const STATUS_LOCKED = 423;
    //    public const STATUS_FAILED_DEPENDENCY = 424;
    //    public const STATUS_TOO_EARLY = 425;
    //    public const STATUS_UPGRADE_REQUIRED = 426;
    //    public const STATUS_PRECONDITION_REQUIRED = 428;
    //    public const STATUS_REQUEST_HEADER_FIELDS_TOO_LARGE = 431;
    //    public const STATUS_UNAVAILABLE_FOR_LEGAL_REASONS = 451;

    //    public const STATUS_NOT_IMPLEMENTED = 501;
    //    public const STATUS_BAD_GATEWAY = 502;
    //    public const STATUS_SERVICE_UNAVAILABLE = 503;
    //    public const STATUS_GATEWAY_TIMEOUT = 504;
    //    public const STATUS_HTTP_VERSION_NOT_SUPPORTED = 505;
    //    public const STATUS_VARIANT_ALSO_NEGOTIATES = 506;
    //    public const STATUS_INSUFFICIENT_STORAGE = 507;
    //    public const STATUS_LOOP_DETECTED = 508;
    //    public const STATUS_NOT_EXTENDED = 510;
    //    public const STATUS_NETWORK_AUTHENTICATION_REQUIRED = 511;
}