<?php

namespace Retrocode\Souzou\Service;

use DateTimeImmutable;
use DateTimeZone;

/**
 * Wrapper for Date Time operations
 *
 * Note: this is immutable, so each chained operation returns a new instance, leaving the
 *       original untouched
 */
class DateTimeService
{
    /** @var DateTimeImmutable */
    private $dateTime;

    public function __construct(string $datetime = 'now', ?DateTimeZone $timezone = null)
    {
        $timezone       = $timezone ?? new DateTimeZone('UTC');
        $this->dateTime = new DateTimeImmutable($datetime, $timezone);
    }

    public static function fromFormat(string $dateString, string $format, ?DateTimeZone $timezone = null): self
    {
        $timezone = $timezone ?? new DateTimeZone('UTC');
        $dateTime = DateTimeImmutable::createFromFormat($format, $dateString, $timezone);

        if ($dateTime === false) {
            throw new \InvalidArgumentException("Invalid date string or format: {$dateString}, {$format}");
        }

        return new self($dateTime->format('Y-m-d H:i:s'), $timezone);
    }

    public function modify(string $dateTimeOffset): self
    {
        return new self($this->dateTime->modify($dateTimeOffset)->format(DateTimeImmutable::ATOM),
                        $this->dateTime->getTimezone());
    }

    /**
     * Ignoring the time element, return true if the dates match
     */
    public function isSameDate(DateTimeService $dateTime): bool
    {
        return $this->toMySqlDate() === $dateTime->toMySqlDate();
    }

    public function now(): self
    {
        return new self('now', $this->dateTime->getTimezone());
    }

    public function setTime(int $hours, int $minutes, int $seconds): self
    {
        return new self($this->format(sprintf("Y-m-d %d:%d:%d", $hours, $minutes, $seconds)));
    }

    public function humanReadableDate(): string
    {
        return $this->dateTime->format('l, d m Y');
    }

    public function humanReadableDateTime(): string
    {
        return $this->dateTime->format('l, d M Y H:i:s');
    }

    public function year(): int
    {
        return $this->dateTime->format('Y');
    }

    public function month(): int
    {
        return $this->dateTime->format('m');
    }

    /**
     * Return the day of the month (e.g. 1-31)
     */
    public function day(): int
    {
        return $this->dateTime->format('d');
    }

    /**
     * Return Monday, Tuesday, Wednesdat, etc
     */
    public function weekday(): string
    {
        return $this->dateTime->format('l');
    }

    public function addDays(int $days): self
    {
        return new self($this->dateTime->modify("+{$days} days")->format('Y-m-d H:i:s'),
                        $this->dateTime->getTimezone());
    }

    public function subDays(int $days): self
    {
        return new self($this->dateTime->modify("-{$days} days")->format('Y-m-d H:i:s'),
                        $this->dateTime->getTimezone());
    }

    public function addHours(int $hours): self
    {
        return new self($this->dateTime->modify("+{$hours} hours")->format('Y-m-d H:i:s'),
                        $this->dateTime->getTimezone());
    }

    public function subHours(int $hours): self
    {
        return new self($this->dateTime->modify("-{$hours} hours")->format('Y-m-d H:i:s'),
                        $this->dateTime->getTimezone());
    }

    public function toMySqlDate(): string
    {
        return $this->dateTime->format('Y-m-d');
    }

    public function toMySqlDateTime(): string
    {
        return $this->dateTime->format('Y-m-d H:i:s');
    }

    public function toIsoDateTime(): string
    {
        return $this->dateTime->format(DateTimeImmutable::ATOM);
    }

    public function toTimestamp(): int
    {
        return $this->dateTime->getTimestamp();
    }

    public function format(string $format): string
    {
        return $this->dateTime->format($format);
    }

    /**
     * Return the day of the week, e.g. "Monday", "Tuesday", etc
     */
    public function dayOfWeek(): string
    {
        return $this->dateTime->format('l');
    }

    public function setTimezone(DateTimeZone $timezone): self
    {
        return new self($this->dateTime->format('Y-m-d H:i:s'), $timezone);
    }
}