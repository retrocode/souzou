<?php

namespace Retrocode\Souzou\Service;

class ClassFinderService
{
    /**
     * @return string[]
     */
    function findClassesThatExtend(string $baseClass, string $folder): array
    {
        $classes = [];

        // Get all files in the specified folder and its subfolders
        $iterator = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($folder)
        );

        // Filter files to include only PHP files
        $phpFiles = new RegexIterator($iterator, '/\.php$/');

        foreach ($phpFiles as $file) {
            $filepath = $file->getRealPath();

            // Get the namespace and class name from the file
            $tokens    = token_get_all(file_get_contents($filepath));
            $namespace = '';
            $classname = '';

            foreach ($tokens as $token) {
                if (is_array($token) && $token[ 0 ] === T_NAMESPACE) {
                    $namespace = '';
                    while (( $token = next($tokens) ) !== false) {
                        if (is_string($token)) {
                            if ($token === ';') {
                                break;
                            }
                            $namespace .= $token;
                        }
                    }
                }

                if (is_array($token) && $token[ 0 ] === T_CLASS) {
                    $token = next($tokens);
                    if (is_array($token) && $token[ 0 ] === T_WHITESPACE) {
                        $token = next($tokens);
                    }
                    if (is_array($token) && $token[ 0 ] === T_STRING) {
                        $classname = $token[ 1 ];
                        break;
                    }
                }
            }

            if ( ! empty($classname)) {
                $fullyQualifiedClassName = $namespace . '\\' . $classname;

                // Check if the class extends the base class
                if (is_subclass_of($fullyQualifiedClassName, $baseClass)) {
                    $classes[] = $fullyQualifiedClassName;
                }
            }
        }

        return $classes;
    }
}