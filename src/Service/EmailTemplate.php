<?php

namespace Retrocode\Souzou\Service;

abstract class EmailTemplate
{
    /** @var string|null */
    protected $message = null;
    /** @var string|null */
    protected $subject = null;
    /** @var array|null */
    protected $placeholders = null;

    /**
     * Return the content of the HTML-formatted email
     * Note: this can contain placeholders in the format "{{placeholderName}}"
     *       which will be compiled before fetching
     */
    abstract protected function getHtmlTemplate(): string;

    public function getTextContent(): string
    {
        if ( ! $this->message) {
            throw new \RuntimeException('Email content not defined');
        }

        return strip_tags($this->message);
    }

    final public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    final public function setPlaceholders(array $placeholders): self
    {
        $this->placeholders = $placeholders;

        return $this;
    }

    final public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    final public function getHtmlContent(): string
    {
        if ( ! $this->message) {
            throw new \RuntimeException('Email message not defined');
        }

        return $this->compile($this->getTemplateWithMessage(), $this->placeholders);
    }

    final public function getSubject(): string
    {
        if ( ! $this->subject) {
            throw new \RuntimeException('Email subject not defined');
        }

        return $this->compile($this->subject, $this->placeholders);
    }

    private function getTemplateWithMessage(): string
    {
        return $this->compile($this->getHtmlTemplate(), [ 'message' => $this->message ]);
    }

    private function compile(string $textWithPlaceholders, array $placeholders = null): string
    {
        $placeholders = $placeholders ?? $this->placeholders;

        if (empty($placeholders)) {
            return $textWithPlaceholders;
        }

        $stringContent = $textWithPlaceholders;

        foreach ($placeholders as $key => $value) {
            $stringContent = str_replace('{{' . $key . '}}', $value, $stringContent);
        }

        return $stringContent;
    }
}