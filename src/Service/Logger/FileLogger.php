<?php

namespace Retrocode\Souzou\Service\Logger;

use Retrocode\Souzou\Service\AppSettings;
use Retrocode\Souzou\Service\Logger;
use RuntimeException;

class FileLogger implements Logger
{

    /** @var resource */
    private $file;

    public function __construct(AppSettings $configManager)
    {
        $this->file = $this->initFile($configManager->getString('app.logFile'));
    }

    public function __destruct()
    {
        fflush($this->file);
        fclose($this->file);
    }

    public function info(string $message): void
    {
        fwrite($this->file, sprintf("[%s] INFO:  %s" . PHP_EOL, date('Y-m-d H:i:s'), $message));
    }

    public function error(string $message): void
    {
        fwrite($this->file, sprintf("[%s] ERROR: %s" . PHP_EOL, date('Y-m-d H:i:s'), $message));
        fflush($this->file);
    }

    private function initFile(?string $logFile)
    {
        if ($logFile === null) {
            throw new RuntimeException('No app.logFile has been defined in the config');
        }

        $file = fopen($logFile, 'a');

        if ( ! $file) {
            throw new RuntimeException(sprintf("Could not initialise file '%s' for writing", $logFile));
        }

        return $file;
    }
}