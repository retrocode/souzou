<?php

namespace Retrocode\Souzou\Service\Logger;

use Retrocode\Souzou\Service\Logger;

class NullLogger implements Logger
{

    public function info(string $message): void
    {
        // Do nothing
    }

    public function error(string $message): void
    {
        // DO nothing
    }
}