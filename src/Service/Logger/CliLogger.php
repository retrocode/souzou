<?php

namespace Retrocode\Souzou\Service\Logger;

use Retrocode\Souzou\Service\Logger;

class CliLogger implements Logger
{

    public function info(string $message): void
    {
        echo $message . PHP_EOL;
    }

    public function error(string $message): void
    {
        fwrite(STDERR, $message . PHP_EOL);
    }
}