<?php

namespace Retrocode\Souzou\Service;

use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use Retrocode\Souzou\IO\Http\HttpResponse;

/**
 * Mail Service for sending emails using the settings in AppSettings
 *
 * Configuration notes for local testing (in Linux)
 * ------------------------------------------------
 * Install msmtp
 * Create/edit /etc/msmtprc with contents:
 * ---
 * defaults
 * tls off
 *
 * account maildev
 * host localhost
 * port 1025
 * from test@example.com
 *
 * account default: maildev
 * ---
 *
 * Edit /etc/php/php.ini and add "sendmail_path = /usr/bin/msmtp -t"
 * Test with: /usr/bin/msmtp -t "test"
 *
 * Run maildev and load localhost:1080 in the browser
 *
 * @deprecated Needs a mailer library
 */
class MailService
{

    /** @var PHPMailer */
    private $phpMailer;
    /** @var AppSettings */
    private $configManager;
    /** @var string|null */
    private $subject = null;
    /** @var EmailTemplate */
    private $emailTemplate;
    /** @var string|null */
    private $toEmail;
    /** @var string|null */
    private $toName;

    public function __construct(AppSettings $configManager, EmailTemplate $emailTemplate)
    {
        $this->phpMailer     = new PHPMailer(true);
        $this->configManager = $configManager;
        $this->emailTemplate = $emailTemplate;
    }

    public function setPlaceholders(array $placeholders): self
    {
        $this->emailTemplate->setPlaceholders($placeholders);

        return $this;
    }

    public function setMessage(string $message): self
    {
        $this->emailTemplate->setMessage($message);

        return $this;
    }

    public function to(string $toEmail, string $toName = null): self
    {
        $this->toEmail = $toEmail;
        $this->toName  = $toName;

        return $this;
    }

    public function setSubject(string $subject): self
    {
        $this->emailTemplate->setSubject($subject);

        return $this;
    }

    // TODO: We are likely to need to add CC and BCC as options
    public function send(): bool
    {
        $this->configureMailer();

        echo 'html content: ' . $this->emailTemplate->getHtmlContent();

        // Set the content of the email
        $this->phpMailer->Subject = $this->emailTemplate->getSubject();
        $this->phpMailer->Body    = $this->emailTemplate->getHtmlContent();
        $this->phpMailer->AltBody = $this->emailTemplate->getTextContent();

        $fromName    = $this->configManager->getString('email.from_name');
        $fromAddress = $this->configManager->getString('email.from_address');

        try {
            $this->phpMailer->setFrom($fromAddress, $fromName);
            $this->phpMailer->addAddress($this->toEmail, $this->toName);

            return $this->phpMailer->send();
        } catch (Exception $e) {
            throw new \RuntimeException(
                sprintf("Email exception: %s - %s", $e->getMessage(), $this->phpMailer->ErrorInfo),
                HttpResponse::STATUS_INTERNAL_SERVER_ERROR,
                $e);
        }

    }

    public function configureMailer(): void
    {
        $email = $this->configManager->get('email');
        $debug = (bool) $email[ 'debug' ] ?? false;

        // Based on functions.php
        $this->phpMailer->isSMTP();
        $this->phpMailer->CharSet = 'UTF-8';
        $this->phpMailer->Host    = $email[ 'smtp_host' ];
        $this->phpMailer->Port    = (int) $email[ 'smtp_port' ];


        // Setup SMTP
        $smtpAuth                  = (bool) ( $email[ 'smtp_auth' ] ) ?? true;
        $this->phpMailer->SMTPAuth = $smtpAuth;
        if ($smtpAuth) {
            $this->phpMailer->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $this->phpMailer->Username   = $email[ 'smtp_username' ];
            $this->phpMailer->Password   = $email[ 'smtp_password' ];

            $this->phpMailer->SMTPDebug = $debug;
            if ($emailSettings[ 'smtp_auto_tls' ] ?? null) {
                $this->mail->SMTPAutoTLS = $emailSettings[ 'smtp_auto_tls' ];
            }
        }

        // Set to HTML email format
        $this->phpMailer->isHTML();
    }


}