<?php

namespace Retrocode\Souzou\Service\HtmlTemplateEngine;

/**
 * Engine for rendering HTML from templates
 *
 * May be BladeOne, Twig or Smarty (or others if implementing this)
 */
interface TemplateEngine
{
    /**
     * Render the named template and return the HTML
     *
     * @param  string  $name
     * @param  array   $context
     *
     * @return string
     */
    public function render(string $name, array $context = []): string;

    /**
     * Clear any compiled template cache files
     *
     * @return string
     */
    public function clearCache(): string;
}