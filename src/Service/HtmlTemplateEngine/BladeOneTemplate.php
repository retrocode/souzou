<?php

namespace Retrocode\Souzou\Service\HtmlTemplateEngine;

use eftec\bladeone\BladeOne;
use Retrocode\Souzou\Service\AppSettings;

/**
 * Wraps basic functionality for BladeOne, a Blade clone that's fully compatible
 * with Laravel's Blade library, but without any Laravel requirement.
 *
 * Note: it doesn't support dynamic elements.
 */
class BladeOneTemplate implements TemplateEngine
{
    private BladeOne $blade;

    public function __construct(AppSettings $appSettings)
    {
        $this->blade = new BladeOne($appSettings->get('template.path'),
                                    $appSettings->get('template.cache_path'),
                                    BladeOne::MODE_DEBUG);
        $this->blade->share('app_name', $appSettings->get('app.name'));
        $this->blade->share('media_url', $appSettings->get('app.media_url'));
        $this->blade->share('base_url', $appSettings->get('app.base_url'));
    }

    public function render(string $name, array $context = []): string
    {
        return $this->blade->run($name, $context);
    }

    public function clearCache(): string
    {
        return $this->blade->clearcompile();
    }
}