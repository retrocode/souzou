<?php

namespace Retrocode\Souzou\Service;

/**
 * Logging for output - can be sent to the CLI or DB for debugging, or a "black hole" one for no output
 */
interface Logger
{
    public function info(string $message): void;

    public function error(string $message): void;
}