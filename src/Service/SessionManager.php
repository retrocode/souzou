<?php

namespace Retrocode\Souzou\Service;

/**
 * @deprecated This needs project-specific code extracted to be used
 */
class SessionManager
{
    /** @var string */
    private $portalSessionPrefix;

    public function __construct(AppSettings $appSettings)
    {
        $this->portalSessionPrefix = $appSettings->getString('app.portal_sec');

        if (!$this->portalSessionPrefix) {
            throw new \RuntimeException('Session prefix ("app.portal_sec") not found in the config');
        }

        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }
    }

    public function getSessionKey(): ?string
    {
        return $this->get($this->portalSessionPrefix . 'session_key');
    }

    public function getUserLevel(): ?int
    {
        $userLevel = $this->get($this->portalSessionPrefix . 'session_user_level');

        if ($userLevel === null) {
            return null;
        }

        return (int)$userLevel;
    }

    public function set(string $key, $value): void
    {
        $_SESSION[ $key ] = $value;
    }

    /**
     * @return mixed|null
     */
    public function get(string $key, $default = null)
    {
        return $_SESSION[ $key ] ?? $default;
    }

    public function has(string $key): bool
    {
        return isset($_SESSION[ $key ]);
    }

    public function remove(string $key): void
    {
        unset($_SESSION[ $key ]);
    }

    public function clear(): void
    {
        $_SESSION = [];
    }

    public function destroy(): void
    {
        session_destroy();
    }
}