<?php

namespace Retrocode\Souzou\Service;

use Retrocode\Souzou\Contract\IResolveValues;

/**
 * Configuration manager for the application.
 *
 * This class is responsible for loading and managing the application's configuration settings.
 * It allows retrieving configuration values using a simple and intuitive API.
 *
 * The configuration is loaded from PHP files that return associative arrays. The class supports
 * a base configuration file and environment-specific configuration files to override or extend
 * the base configuration.
 *
 * Example usage:
 * ```
 * $configManager = new AppSettings('dev');
 * $dbConfig = $configManager->get('db');
 * $appName = $configManager->get('app.name');
 * $debugMode = $configManager->get('app.debug', false);
 * ```
 */
class AppSettings implements IResolveValues
{
    private array  $config = [];
    private bool   $isProduction;
    private string $configFilesPath;
    private array  $routeParams;

    public function __construct(string $configFilesPath, string $isDev)
    {
        if ( ! file_exists($configFilesPath)) {
            throw new \RuntimeException(sprintf('Config files path %s not found', $configFilesPath));
        }

        $this->configFilesPath = $configFilesPath;
        $this->isProduction    = ! $isDev;

        $this->loadConfig();
    }

    public function isEnvironmentProduction(): bool
    {
        return $this->isProduction;
    }

    public function isEnvironmentDevelopment(): bool
    {
        return ! $this->isProduction;
    }

    public function baseUrl(): string
    {
        $url = $this->get('app.base_url');

        if ($url === null) {
            throw new \RuntimeException('Application URL not set in configuration');
        }

        return rtrim($url, '/');
    }

    public function getEnvValue(string $name): string | null
    {
        return $_ENV[ strtoupper($name) ] ?? null;
    }

    public function get(string $key, $default = null): mixed
    {
        return $this->getNestedValue($this->config, $key, $default);
    }

    public function getString(string $key): ?string
    {
        return (string) $this->get($key, null);
    }

    public function getInt(string $key): ?int
    {
        return (int) $this->get($key, null);
    }

    public function getBool(string $key, bool $default): bool
    {
        return (bool) $this->get($key, $default);
    }

    private function loadConfig(): void
    {
        // Load in the framework's config
        $baseConfig   = require __DIR__ . '/../../config/config.php';
        $this->config = $baseConfig;

        // Load in the app's own configuration to override the framework's defaults
        $envConfig    = $this->fetchFromConfigFile('config.php', true);
        $this->config = array_replace_recursive($this->config, $envConfig);

        // Allow separation of configs, if desired (likely phased out
        $envConfig    = $this->fetchFromConfigFile('routes.php');
        $this->config = array_replace_recursive($this->config, $envConfig);

        $envConfig    = $this->fetchFromConfigFile('cli_commands.php');
        $this->config = array_replace_recursive($this->config, $envConfig);

        $envConfig    = $this->fetchFromConfigFile('middleware.php');
        $this->config = array_replace_recursive($this->config, $envConfig);

        $envConfig    = $this->fetchFromConfigFile('config_dev.php',);
        $this->config = array_replace_recursive($this->config, $envConfig);

        $envConfig    = $this->fetchFromConfigFile('config.local.php',);
        $this->config = array_replace_recursive($this->config, $envConfig);
    }

    private function fetchFromConfigFile(string $configFileName, bool $isRequired = false): array
    {
        $filePath = $this->configFilesPath . DIRECTORY_SEPARATOR . $configFileName;
        if ( ! file_exists($filePath)) {
            if ($isRequired) {
                throw new \RuntimeException(sprintf("Required config file '%s' not found", $configFileName));
            }

            return [];
        }

        return require $filePath;
    }

    private function getNestedValue(array $array, ?string $key, mixed $default = null)
    {
        if (is_null($key)) {
            return $array;
        }

        if (isset($array[ $key ])) {
            return $array[ $key ];
        }

        foreach (explode('.', $key) as $segment) {
            if ( ! is_array($array) || ! array_key_exists($segment, $array)) {
                return $default;
            }

            $array = $array[ $segment ];
        }

        return $array;
    }

    public function setRouteParams(array $params): void
    {
        $this->routeParams = $params;
    }

    public function getRouteParam(string $key, mixed $default): mixed
    {
        return $this->routeParams[ $key ] ?? $default;
    }


}