<?php

declare( strict_types=1 );

namespace Retrocode\Souzou\Service;

use ReflectionException;
use ReflectionFunction;
use ReflectionIntersectionType;
use ReflectionNamedType;
use ReflectionParameter;
use ReflectionUnionType;
use Retrocode\Souzou\Exception\ValueNotResolvedException;
use RuntimeException;

/**
 * Container class for dependency injection.
 *
 * This class provides a simple implementation of a dependency injection container.
 * It allows registering and resolving dependencies, as well as invoking callables
 * with automatic dependency resolution.
 */
class Container
{
    private array $instances = [];
    /** @var array Abstract class to instance or callable map */
    private array $diMappings = [];
    /** @var array ReflectionParameter to key converter functions, keyed by the IResolveValues class name */
    private array $valueResolvers = [];

    public function __construct() {}

    /**
     * Sets a resolver for scalar method (constructor) arguments.
     *
     * Could be used for config, request, cli, etc
     *
     * e.g. for using $appSettings->get($keyName) against __construct(private string $configKeyName)
     *
     * Note: The converter should throw a ValueNotResolvedException on failure (null may be a valid value)
     *
     * @param  string    $iResolveValuesClassName
     * @param  callable  $reflectionParameterToKeyConverter  Function to extract the value from the setter
     *                                                       $converter($resolver, $reflectionParameter);
     */
    public function addValueResolver(string $iResolveValuesClassName, callable $reflectionParameterToKeyConverter): void
    {
        $this->valueResolvers[ $iResolveValuesClassName ] = $reflectionParameterToKeyConverter;
    }

    /**
     * Add a DI mapping from an abstract class or interface to a concrete classname, or instance create method
     */
    public function addMapping(string $abstractOrInterface, callable | string $concreteClassNameOrInstantiator): void
    {
        $this->diMappings[ $abstractOrInterface ] = $concreteClassNameOrInstantiator;
    }

    /**
     * Set a value in the container
     */
    public function set(string $key, mixed $value): void
    {
        $this->instances[ $key ] = $value;
    }

    /**
     * Get a singleton instance of a class, instantiated
     *
     * If it's passed a classname that hasn't previously been created, then it will
     * attempt to instantiate it.
     *
     * @param  string  $keyOrClassName
     *
     * @return object|null
     * @throws ReflectionException
     */
    public function get(string $keyOrClassName): ?object
    {
        $instance = $this->instances[ $keyOrClassName ] ?? null;
        if (is_object($instance) && ! is_callable($instance)) {
            return $instance;
        }

        if (is_null($instance)) {
            $this->instances[ $keyOrClassName ] = $this->instantiateClass($keyOrClassName);

            return $this->get($keyOrClassName);
        }

        if (is_callable($instance)) {
            $this->instances[ $keyOrClassName ] = ( $instance )();

            return $this->get($keyOrClassName);
        }

        if (class_exists($instance)) {
            $this->instances[ $keyOrClassName ] = $this->instantiateClass($instance);

            return $this->get($keyOrClassName);

        }

        return ( is_object($instance) ) ? $instance :
            throw new RuntimeException(sprintf("No instance found for '%s'", $keyOrClassName));
    }

    /**
     * Check if the container has a particular key or class
     */
    public function has(string $key): bool
    {
        return isset($this->instances[ $key ]) || isset($this->diMappings[ $key ]) || class_exists($key);
    }

    /**
     * Get an instance of the specified class with constructor dependencies resolved.
     *
     * @param  string  $concreteClassName  The name of the class to instantiate.
     *
     * @return object The instance of the specified class.
     * @throws \ReflectionException If the class does not exist or has invalid constructor parameters.
     */
    public function instantiateClass(string $concreteClassName): object
    {
        if (isset($this->instances[ $concreteClassName ])) {
            return $this->instances[ $concreteClassName ];
        }
        $reflectionClass = new \ReflectionClass($concreteClassName);

        if ( ! $reflectionClass->isInstantiable()) {
            throw new RuntimeException("Class $concreteClassName is not instantiable.");
        }

        $constructor = $reflectionClass->getConstructor();

        if ($constructor === null) {
            return $reflectionClass->newInstance();
        }

        $parameters   = $constructor->getParameters();
        $dependencies = array_map(
            fn(ReflectionParameter $parameter) => $this->resolveParameter($parameter, $concreteClassName),
            $parameters
        );

        $instance                              = $reflectionClass->newInstanceArgs($dependencies);
        $this->instances[ $concreteClassName ] = $instance;

        return $instance;
    }

    /**
     * Resolve a parameter for dependency injection
     *
     * @throws RuntimeException If the parameter cannot be resolved
     */
    private function resolveParameter(ReflectionParameter $parameter, string $className): mixed
    {
        $parameterType = $parameter->getType();
        switch (true) {
            case $parameterType instanceof ReflectionNamedType && ! $parameterType->isBuiltin():
                return $this->get($parameterType->getName());
            case $parameterType instanceof ReflectionUnionType:
                return $this->resolveUnionType($parameterType, $parameter, $className);
            case $parameterType instanceof ReflectionIntersectionType:
                return $this->resolveIntersectionType($parameterType, $parameter, $className);
            case ( $value = $this->attemptToResolveScalarValue($parameter, $className) ) !== null:
                return $value;
            case $parameter->isDefaultValueAvailable():
                return $parameter->getDefaultValue();
            case $parameterType === null:
                return throw new RuntimeException(sprintf("Parameter '%s' of class '%s' does not have a type hint.",
                                                          $parameter->getName(),
                                                          $className));
            default:
                return throw new RuntimeException("Unable to resolve parameter '{$parameter->getName()}' for class $className");
        }
    }

    /**
     * Attempt to resolve a scalar value using registered value resolvers
     */
    private function attemptToResolveScalarValue(ReflectionParameter $parameter,
                                                 string              $className,
                                                 bool                &$wasResolved = false): mixed
    {
        $parameterType = $parameter->getType();
        if ( ! $parameterType instanceof ReflectionNamedType ||
             ! in_array($parameterType->getName(), [ 'string', 'float', 'int', 'bool' ], true)) {
            return null;
        }

        foreach ($this->valueResolvers as $iResolveValuesClass => $argumentToValueConverter) {
            $valueResolver = $this->get($iResolveValuesClass);

            if ( ! $valueResolver) {
                continue;
            }

            try {
                $wasResolved = true;

                return ( $argumentToValueConverter )($parameter, $className, $valueResolver);
            } catch (ValueNotResolvedException) {
                // Continue to the next resolver
                $wasResolved = false;
            }
        }

        return null;
    }

    /**
     * Call a callable with automatic dependency resolution
     *
     * @throws ReflectionException
     * @throws RuntimeException
     */
    public function call(callable | array $callable, array $parameters = []): mixed
    {
        $reflectionFunction = is_array($callable)
            ? new \ReflectionMethod($callable[ 0 ], $callable[ 1 ])
            : new ReflectionFunction($callable);

        $reflectionParameters = $reflectionFunction->getParameters();

        $resolvedParameters = [];
        foreach ($reflectionParameters as $parameter) {
            $parameterName                        = $parameter->getName();
            $resolvedParameters[ $parameterName ] =
                $parameters[ $parameterName ]
                ??
                $this->resolveParameter($parameter, $reflectionFunction->getDeclaringClass()?->getName() ?? '');
        }

        return $reflectionFunction->invokeArgs($callable[ 0 ] ?? null, $resolvedParameters);
    }

    /**
     * Resolve a union type parameter
     *
     * @throws RuntimeException If no type in the union can be resolved
     */
    private function resolveUnionType(ReflectionUnionType $unionType,
                                      ReflectionParameter $parameter,
                                      string              $className): mixed
    {
        foreach ($unionType->getTypes() as $type) {
            if ($type instanceof ReflectionNamedType && ! $type->isBuiltin()) {
                try {
                    return $this->get($type->getName());
                } catch (RuntimeException) {
                    // Continue to the next type
                }
            }
        }

        throw new RuntimeException("Unable to resolve union type parameter '{$parameter->getName()}' for class $className");
    }

    /**
     * Resolve an intersection type parameter
     *
     * @throws RuntimeException If the intersection type cannot be resolved
     */
    private function resolveIntersectionType(ReflectionIntersectionType $intersectionType,
                                             ReflectionParameter        $parameter,
                                             string                     $className): object
    {
        $types     = $intersectionType->getTypes();
        $firstType = $types[ 0 ];

        if ( ! $firstType instanceof ReflectionNamedType || $firstType->isBuiltin()) {
            throw new RuntimeException("Unable to resolve intersection type parameter '{$parameter->getName()}' for class $className");
        }

        $instance = $this->get($firstType->getName());

        // Verify that the instance satisfies all types in the intersection
        foreach ($types as $type) {
            $name = $type->getName();
            if ( ! $type instanceof ReflectionNamedType || $instance instanceof $name) {
                throw new RuntimeException("Resolved instance does not satisfy all types in intersection for parameter '{$parameter->getName()}' of class $className");
            }
        }

        return $instance;
    }

}