<?php

namespace Retrocode\Souzou\Service;

use PDO;
use RuntimeException;

class DatabaseConnection
{
    private PDO $connection;

    public function __construct(AppSettings $config)
    {
        $dbConfig = $config->get('db');

        try {
            $dsn      = "mysql:host={$dbConfig['host']};dbname={$dbConfig['database']};port={$dbConfig['port']}";
            $username = $dbConfig[ 'username' ];
            $password = $dbConfig[ 'password' ];

            // Use PDO as we can use named parameters for queries
            $this->connection = new PDO($dsn, $username, $password);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $exception) {
            throw new RuntimeException('Database connection failed: ' . $exception->getMessage());
        }
    }

    /**
     * Fetch a single item from the database
     * 
     * @param  string  $sql
     * @param  array   $params
     *
     * @return array|null
     */
    public function fetch(string $sql, array $params = []): ?array
    {
        $statement = $this->connection->prepare($sql);

        foreach ($params as $key => $value) {
            $statement->bindValue(":$key", $value);
        }

        $statement->execute();

        return $statement->fetch(PDO::FETCH_ASSOC);
    }

    public function query(string $sql, array $params = []): ?array
    {
        $statement = $this->connection->prepare($sql);

        foreach ($params as $key => $value) {
            $statement->bindValue(":$key", $value);
        }

        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Execute a raw query (with no return value required)
     */
    public function execute(string $sql, array $params = []): void
    {
        $statement = $this->connection->prepare($sql);

        foreach ($params as $key => $value) {
            $statement->bindValue(":$key", $value);
        }

        $statement->execute();
    }

    public function lastInsertId(): ?int
    {
        return $this->connection->lastInsertId() ?: null;
    }

    public function beginTransaction(): void
    {
        $this->connection->beginTransaction();
    }

    public function commit(): void
    {
        $this->connection->commit();
    }

    public function rollback(): void
    {
        $this->connection->rollback();
    }
}