<?php

namespace Retrocode\Souzou;

use ReflectionException;
use Retrocode\Souzou\CliCommand\HelpCommand;
use Retrocode\Souzou\Event\EventDispatcher;
use Retrocode\Souzou\IO\Cli\CliCommand;
use Retrocode\Souzou\IO\Cli\CliInput;
use Retrocode\Souzou\IO\Cli\CliOutput;
use Retrocode\Souzou\IO\Cli\CliRequest;
use Retrocode\Souzou\IO\Http\HttpRequest;
use Retrocode\Souzou\Routing\Router;
use Retrocode\Souzou\Service\AppSettings;
use Retrocode\Souzou\Service\Container;

/**
 * Configure the application - routes, logging, email templates, middleware and events
 *
 * Calling run, will check routing and send a suitable response
 */
class Framework
{
    private AppSettings $appSettings;
    /** @var Container For dependency injection */
    private Container $container;
    /** @var EventDispatcher to add events and listeners */
    private EventDispatcher $eventDispatcher;

    public function __construct(string $configFilesPath, bool $isDev = false)
    {
        // Initialise core Framework classes
        $this->appSettings     = new AppSettings($configFilesPath, $isDev);
        $this->container       = new Container($this->appSettings);
        $this->eventDispatcher = new EventDispatcher($this->container);

        $this->container->set(AppSettings::class, $this->appSettings);

        $this->addContainerMappings();
        $this->addValueResolvers();

        // Add event tracking/dispatching
        // TODO: Needs testing
        $this->container->set(EventDispatcher::class, $this->eventDispatcher);
    }

    public function run(): void
    {
        try {
            /** @var Router $router */
            $router = $this->container->instantiateClass(Router::class);

            $this->defineRoutes($router);
            $this->addMiddleware($router);
            $this->configureEventListeners();

            // Dispatch the request
            $request  = new HttpRequest();
            $response = $router->dispatch($request, $this->container);

            // Send the response
            $response->send();
        } catch (ReflectionException $e) {
            printf("Error with the auto loading: %s" . PHP_EOL, $e->getMessage());
        }
    }

    public function runCommand(): void
    {
        try {
            // TODO: Might want middleware for CLI commands
            $this->configureEventListeners();

            $request      = new CliRequest();
            $commands     = $this->appSettings->get('cli_commands');
            $commandClass = $commands[ $request->getArgument(0) ] ?? HelpCommand::class;

            /** @var CliCommand $command */
            $command = $this->container->instantiateClass($commandClass);

            // Dispatch the request
            $command->withInput(new CliInput());
            $command->withOutput(new CliOutput());

            $command->run($request);
        } catch (ReflectionException $e) {
            printf("Error with the auto loading: %s" . PHP_EOL, $e->getMessage());
        }
    }

    /**
     * Define routes to use with the application.
     * Note: method names may be supported, but not certain if recommended yet (prefer focused controllers)
     */
    private function defineRoutes(Router $router): void
    {
        $routes = $this->appSettings->get('routes');
        foreach ($routes as $route => $className) {
            if ( ! str_contains($route, ':')) {
                $method = 'GET';
            } else {
                [ $method, $route ] = explode(':', $route, 2);
            }

            $router->addRoute($method, $route, $className);
        }
    }

    /**
     * Define middleware
     * Middleware can wrap functionality around route handlers (controllers), e.g. for logging
     * or db transactions, etc
     */
    private function addMiddleware(Router $router): void
    {
        $middleware = $this->appSettings->get('middleware');

        foreach ($middleware as $middlewareClassName) {
            $router->addMiddleware($this->container->instantiateClass($middlewareClassName));
        }
    }

    private function configureEventListeners()
    {
        // TODO: Load these from the config "event_listeners" >> 'eventName' =>  Listener::class
        // Add event listener classes here - e.g. to send an email after an order is confirmed, etc
//        $this->eventDispatcher->addListener('user.added', EventListener::eventName);
    }

    /**
     * Set the container mappings from a config passed
     */
    private function addContainerMappings(): void
    {
        $containerDiMappings = $this->appSettings->get('di_mappings', []);

        foreach ($containerDiMappings as $interfaceOrBaseClass => $concreteClassNameOrCallable) {
            $this->container->set($interfaceOrBaseClass, $concreteClassNameOrCallable);
        }
    }

    private function addValueResolvers() {
        $containerDiMappings = $this->appSettings->get('argument_resolvers', []);

        foreach ($containerDiMappings as $resolverClassName => $argumentResolverCallable) {
            $this->container->addValueResolver($resolverClassName, $argumentResolverCallable);
        }
    }


}