<?php

namespace Retrocode\Souzou\Event;

use Retrocode\Souzou\Service\Container;

class EventDispatcher
{
    /** @var array */
    private $listeners = [];
    /** @var Container */
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function addListener(string $eventName, string $listenerClassName): void
    {
        if (!isset($this->listeners[$eventName])) {
            $this->listeners[$eventName] = [];
        }

        $this->listeners[$eventName][] = $listenerClassName;
    }

    public function dispatch(Event $event): void
    {
        $eventName = $event->getName();

        if (isset($this->listeners[$eventName])) {
            foreach ($this->listeners[$eventName] as $listenerClassName) {
                if ($this->container->has($listenerClassName)) {
                    $listener = $this->container->get($listenerClassName);
                } else {
                    $listener =$this->container->instantiateClass($listenerClassName);
                }

                /** @var EventListener $listener */
                $listener->handle($event);
            }
        }
    }
}