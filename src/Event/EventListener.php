<?php

namespace Retrocode\Souzou\Event;

interface EventListener
{
    public function handle(Event $event): void;
}