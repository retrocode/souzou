<?php

namespace Retrocode\Souzou\Contract;

/**
 * Interface for generic handlers.
 *
 * This interface defines the contract for generic handlers in the application.
 * Handlers are responsible for processing specific tasks or actions.
 */
interface HandlerInterface
{
}