<?php

namespace Retrocode\Souzou\Contract;

use Retrocode\Souzou\IO\Http\HttpRequest;
use Retrocode\Souzou\IO\Http\HttpResponse;

/**
 * Interface for controllers.
 *
 * This interface defines the contract for controllers in the application.
 * Controllers are responsible for handling incoming requests, interacting
 * with models or services, and generating appropriate responses.
 */
interface ControllerInterface extends HandlerInterface
{
    /**
     * Handle a request from the client app
     *
     * @return HttpResponse
     */
    public function handle(HttpRequest $request);
}