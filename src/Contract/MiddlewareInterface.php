<?php

namespace Retrocode\Souzou\Contract;

use Retrocode\Souzou\IO\Http\HttpRequest;
use Retrocode\Souzou\IO\Http\HttpResponse;

/**
 * Interface for middleware.
 *
 * This interface defines the contract for middleware components in the application.
 * Middleware are responsible for performing additional processing or modifications
 * to the incoming request or outgoing response.
 */
interface MiddlewareInterface
{
    /**
     * Called before a route handler, allows the middleware to intercept a request and
     * check application state, e.g. to block access or for logging, etc
     *
     * @param  HttpRequest  $request
     * @param  string       $handlerClassname
     *
     * @return HttpResponse|null
     */
    public function preProcess(HttpRequest $request, string $handlerClassname): ?HttpResponse;

    /**
     * Post-processing after a route handler has been executed, and allow the response to
     * be intercepted, for example to transform the results, without affecting the handler.
     *
     * @param  HttpResponse  $response
     * @param  string        $handlerClassname
     *
     * @return HttpResponse|null
     */
    public function postProcess(HttpResponse $response, string $handlerClassname): ?HttpResponse;
}