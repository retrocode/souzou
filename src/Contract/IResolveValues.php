<?php

namespace Retrocode\Souzou\Contract;

interface IResolveValues
{

    public function get(string $key, mixed $defaultValue): mixed;
}