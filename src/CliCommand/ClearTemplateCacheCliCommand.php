<?php

namespace Retrocode\Souzou\CliCommand;

use Retrocode\Souzou\IO\Cli\CliCommand;
use Retrocode\Souzou\IO\Cli\CliRequest;
use Retrocode\Souzou\Service\HtmlTemplateEngine\BladeOneTemplate;

class ClearTemplateCacheCliCommand extends CliCommand
{

    public function __construct(private BladeOneTemplate $bladeTemplate) {}

    public function run(CliRequest $request): int
    {
        $this->bladeTemplate->clearCache();
        $this->output->info('Cache cleared');

        return 0;
    }
}