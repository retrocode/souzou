<?php

namespace Retrocode\Souzou\CliCommand;

use Retrocode\Souzou\IO\Cli\CliCommand;
use Retrocode\Souzou\IO\Cli\CliRequest;
use Retrocode\Souzou\Service\AppSettings;

class HelpCommand extends CliCommand
{

    public function __construct(private AppSettings $appSettings) {}

    public function run(CliRequest $request): int
    {
        $commands = array_keys($this->appSettings->get('cli_commands', []));

        $this->output->info(sprintf("Available commands:" . PHP_EOL . " %s", implode(PHP_EOL, $commands)));

        return 0;
    }
}