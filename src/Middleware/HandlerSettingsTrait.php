<?php

namespace Retrocode\Souzou\Middleware;

use ReflectionClass;

/**
 * Fetches settings for use in middleware, taken from the class doc blocks.
 *
 * Example usage - add to the Handler's class (can be any key/value and used in middleware:
 * Note that the spacing must be exactly as below for this to work
 *
 * @HandlerSetting(key="requiresAuth", value="true")
 * @HandlerSetting(key="userLevel", value="1")
 */
trait HandlerSettingsTrait
{

    public function isImplementation(string $handlerClassName, string $interfaceClassName): bool
    {
        $reflectionClass = new ReflectionClass($handlerClassName);

        return $reflectionClass->implementsInterface($interfaceClassName);
    }

    public function extractHandlerSettings(string $handlerClass): array
    {
        $reflection = new ReflectionClass($handlerClass);
        $docComment = $reflection->getDocComment();

        preg_match_all('/@HandlerSetting\(key="(.+)", value="(.+)"\)/', $docComment, $matches, PREG_SET_ORDER);

        $handlerSettings = [];
        foreach ($matches as $match) {
            $key = $match[1];
            $value = $match[2];
            $handlerSettings[$key] = $value;
        }

        return $handlerSettings;
    }
}