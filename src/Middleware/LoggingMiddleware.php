<?php

namespace Retrocode\Souzou\Middleware;

use Retrocode\Souzou\Contract\MiddlewareInterface;
use Retrocode\Souzou\IO\Http\HttpRequest;
use Retrocode\Souzou\IO\Http\HttpResponse;
use Retrocode\Souzou\Service\Logger;

class LoggingMiddleware implements MiddlewareInterface
{
    /** @var Logger */
    private $logger;

    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function preProcess(HttpRequest $request, string $handlerClassname): ?HttpResponse
    {
        $this->logger->info(sprintf("Started processing %s: %s", $handlerClassname, json_encode($request, JSON_PRETTY_PRINT)));

        return null;
    }

    public function postProcess(HttpResponse $response, string $handlerClassname): ?HttpResponse
    {
        $this->logger->info('Completed processing: '.$handlerClassname);

        return null;
    }
}