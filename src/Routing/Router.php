<?php

namespace Retrocode\Souzou\Routing;

use ReflectionException;
use Retrocode\Souzou\Contract\HandlerInterface;
use Retrocode\Souzou\Contract\MiddlewareInterface;
use Retrocode\Souzou\IO\Http\HttpRequest;
use Retrocode\Souzou\IO\Http\HttpResponse;
use Retrocode\Souzou\IO\Http\Response\HtmlResponse;
use Retrocode\Souzou\Service\AppSettings;
use Retrocode\Souzou\Service\Container;
use RuntimeException;

/**
 * Handles routing and dispatching of HTTP requests.
 *
 * This class is responsible for registering routes, matching incoming requests
 * to the appropriate route, and dispatching the request to the corresponding
 * handler or controller. It also supports middleware execution.
 */
class Router
{
    /** @var array */
    private array $routes = [];
    /** @var MiddlewareInterface[] */
    private array $middleware = [];
    /** @var string */
    private string $urlPath;

    public function __construct(private AppSettings $configManager)
    {
        $baseUrl       = rtrim(parse_url($configManager->getString('app.base_url'), PHP_URL_PATH), '/');
        $this->urlPath = $baseUrl;
    }

    /**
     * Register a route with the router.
     *
     * @param  string  $method   The HTTP method (e.g., GET, POST) for the route.
     * @param  string  $path     The URL path pattern for the route.
     * @param  mixed   $handler  The handler or controller to be invoked when the route matches.
     *
     * @return self The current Router instance for method chaining.
     */
    public function addRoute(string $method, string $path, string $handler): self
    {
        $method = strtoupper($method);
        if ( ! in_array($method, [ 'GET', 'POST' ])) {
            throw new RuntimeException('Unsupported http method: ' . $method);
        }

        $this->routes[] = [
            'method'  => $method,
            'path'    => $path,
            'handler' => $handler,
        ];

        return $this;
    }

    /**
     * Register middleware with the router.
     *
     * @param  MiddlewareInterface  $middleware  Classname or callable for the middleware
     *
     * @return self The current Router instance for method chaining.
     */
    public function addMiddleware(MiddlewareInterface $middleware): self
    {
        $this->middleware[] = $middleware;

        return $this;
    }

    /**
     * Dispatch the incoming request to the appropriate route handler.
     *
     * @param  HttpRequest  $request  The incoming HTTP request.
     *
     * @return HttpResponse The HTTP response generated by the route handler.
     * @throws ReflectionException
     */
    public function dispatch(HttpRequest $request, Container $container): HttpResponse
    {
        $requestMethod = $request->getMethod();
        $requestPath   = $request->getPath();

        // The request path contains everything after the "https://website.com" so we need to
        // extract any relative path (i.e. if the app is running from "/app-name/index.php" then
        // the "/app-name" portion needs to be removed to match the route
        if (str_starts_with($requestPath, $this->urlPath)) {
            $requestPath = substr($requestPath, strlen($this->urlPath));
        }

        // Match the current path, e.g. "/home" with the defined routes
        foreach ($this->routes as $route) {
            if ($route[ 'method' ] !== $requestMethod) {
                continue;
            }

            // TODO: May want to support /todo-list/:id
            $pattern =
                '@^' .
                preg_replace('/\\\:[a-zA-Z0-9\_\-]+/', '([a-zA-Z0-9\-\_]+)', preg_quote($route[ 'path' ])) .
                '$@D';
            if (preg_match($pattern, $requestPath, $matches) === 1) {
                $params = $this->extractParams($route[ 'path' ], $matches);

                $this->configManager->setRouteParams($params);
            } elseif ($route[ 'path' ] !== $requestPath) {
                continue;
            }


            // Execute any middleware prior to calling the route handler
            foreach ($this->middleware as $currentMiddleware) {
                $response = $currentMiddleware->preProcess($request, $route[ 'handler' ]);
                if ($response !== null) {
                    return $response;
                }
            }

            // Execute the route handler
            $response = $this->callHandler($route[ 'handler' ], $container);

            // Execute post-processing middleware after the handler
            foreach ($this->middleware as $currentMiddleware) {
                $postResponse = $currentMiddleware->postProcess($response, $route[ 'handler' ]);
                if ($postResponse !== null) {
                    return $postResponse;
                }
            }

            return $response;
        }

        // TODO: This might return a JSON response, if the request asked for JSON
        return new HtmlResponse('Route not Found: ' . $requestPath, 404);
    }

    private function extractParams($pattern, $matches): array
    {
        $params     = [];
        $paramNames = [];
        preg_match_all('/\\:([a-zA-Z0-9\_\-]+)/', $pattern, $paramNames);
        array_shift($matches);
        foreach ($paramNames[ 1 ] as $index => $paramName) {
            $params[ $paramName ] = $matches[ $index ];
        }

        return $params;
    }

    /**
     * Invoke the route handler with the given request.
     * Note, $handler can be a class name, and additionally may be suffixed with 'classname@methodName'
     *
     * @param  string|HandlerInterface  $handler  The route handler to be invoked.
     * @param  Container                $container
     *
     * @return HttpResponse The HTTP/JSON response generated by the route handler.
     * @throws ReflectionException
     */
    private function callHandler(HandlerInterface | string $handler, Container $container): HttpResponse
    {
        if (is_string($handler)) {
            $parts          = explode('@', $handler);
            $controllerName = $parts[ 0 ];
            $methodName     = $parts[ 1 ] ?? 'handle';

            $controllerInstance = $container->instantiateClass($controllerName);

            return $container->call([ $controllerInstance, $methodName ]);
        }

        if (is_callable($handler)) {
            return $container->call($handler);
        }

        throw new RuntimeException('Invalid route handler');
    }
}